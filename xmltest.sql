/*
MySQL Data Transfer
Source Host: localhost
Source Database: xmltest
Target Host: localhost
Target Database: xmltest
Date: 2009-10-30 21:39:17
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for xml_data
-- ----------------------------
DROP TABLE IF EXISTS `xml_data`;
CREATE TABLE `xml_data` (
  `did` mediumint(8) NOT NULL,
  `f_tid` mediumint(8) NOT NULL,
  `data1` tinytext,
  `data2` tinytext,
  `data3` tinytext,
  PRIMARY KEY  (`did`),
  KEY `index_f` (`f_tid`),
  CONSTRAINT `index_f` FOREIGN KEY (`f_tid`) REFERENCES `xml_type` (`tid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for xml_type
-- ----------------------------
DROP TABLE IF EXISTS `xml_type`;
CREATE TABLE `xml_type` (
  `tid` mediumint(8) NOT NULL COMMENT '主键ID',
  `value` varchar(20) NOT NULL,
  PRIMARY KEY  (`tid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records 
-- ----------------------------
INSERT INTO `xml_data` VALUES ('1', '1', '23232323232', 'rererwer', 'werwerwer'), ('2', '1', '22222222', 'ccccccc', 'cvv'), ('3', '2', '第三方', '大幅度', '第三方'), ('4', '2', '的数据飞机', '答复说', '豕分蛇断'), ('5', '3', '565', 'vcxvc大锅饭', '地方'), ('6', '3', 'gfh传vv', '打得到', '猜猜猜');
INSERT INTO `xml_type` VALUES ('1', '类型一'), ('2', '类型二'), ('3', '类型三');
