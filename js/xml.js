function showData() {
	var type=returnNode("type").value;
	if(type!="-1") {
		var url=base_url+"index.php/welcome/fetchData";
		var postContent="type="+type;
		send_request("post",url,postContent,"text",dataResult);
	}
}

function dataResult() {
	if(http_request.readyState==4) {
		var msg=returnNode("msg");
		removeNode(msg);
		var tbody=document.createElement("tbody");
		if(http_request.status==200) {
			var messages=getXmlDoc(http_request.responseText);
			var datas=messages.getElementsByTagName("data");
			if(datas.length >0) {
				for(var i=0;i<datas.length;i++) {
					var tr=document.createElement("tr");
					var data1=document.createElement("td");
					var data1_obj=datas[i].getElementsByTagName("data1")[0].firstChild;
					var data1_value=data1_obj ? data1_obj.nodeValue : "";
					data1.appendChild(document.createTextNode(data1_value));
					tr.appendChild(data1);
					tbody.appendChild(tr);
				}
				
			} else {
				var tr=document.createElement("tr");
				var td=document.createElement("td");
				td.innerHTML="没找到数据";
				tr.appendChild(td);
				tbody.appendChild(tr);
			}

			msg.appendChild(tbody);
			
		}
	}
}

addLoadEvent(showData);