function send_request(method,url,content,responseType,callback){
     	http_request=false;
     	if(window.XMLHttpRequest){
     		http_request=new XMLHttpRequest();
     		if(http_request.overrideMimeType)
     		   http_request.overrideMimeType("text/xml");
     	}
     	else if(window.ActiveXObject){
     		try{
     			http_request=new ActiveXObject(Msxml2.XMLHTTP);
     		}catch(e){
     			try {
     				http_request=new ActiveXObject("Microsoft.XMLHTTP");
     			}catch(e){}
     		}
     	}
     	if (!http_request){
     		window.alert("不能创建XMLHttpRequest对象实例");
     		return false;
     	}
     	//if(responseType.toLowCase()=="text")
     	if(responseType=="text")
     	    http_request.onreadystatechange=function(){callback();};
     	//else if(responseType.toLowCase()=="xml")
     	else if(responseType=="xml")
     	    http_request.onreadystatechange=function(){callback();};
     	else{
     		window.alert("响应参数类型错误！");
     		return false;
     	}
     	//if(method.toLowCase()=="get"){
     	if(method=="get"){
     		http_request.open(method,url,true);
     	}
     	//else if(method.toLowCase()=="post"){
     	else if(method=="post"){
     		http_request.open(method,url,true);
     		http_request.setRequestHeader("Content-Type","application/x-www-form-urlencoded;charset=utf-8");
     		http_request.setRequestHeader("If-Modified-Since","0");
       }
       else{
       	windows.alert("http请求参数类别错误！");
       	return false;
       }
       http_request.send(content);
     }