function returnNode(id){
	if(!document.getElementById) return false;
	if(!document.getElementById(id)) return false;
	return document.getElementById(id);
}
function removeNode(elementNode){	
	var kids=elementNode.childNodes;
	var kidsNums=kids.length;
	for(var i=kidsNums-1;i>=0;i--){
		elementNode.removeChild(kids[i]);
	}
}
/*
function returnTags(element,tagName){
	if(!document.getElementsByTagName)	return false;
	//alert(element);
	if(!element.getElementsByTagName(tagName))	return false;
	return element.getElementsByTagName(tagName);
}
*/
function insertAfter(newElement,targetElement){
	var parent=targetElement.parentNode;
	//alert(parent);
	if(parent.lastChild==targetElement){
		parent.appendChild(newElement);
	}else{
		parent.insertBefore(newElement,targetElement,nextSibling);
	}
	
}

function addLoadEvent(func){
	var oldonload= window.onload;
	if(typeof window.onload!='function'){
		window.onload=func;
	}else{
		window.onload=function(){
			oldonload();
			func();
		}
	}
}

function moveElement(elementID,final_x,final_y,interval){
	var elem=returnNode(elementID);
	if(elem.movement){
		clearTimeout(elem.movement);
	}
	if(!elem.style.left) elem.style.left="0px";
	if(!elem.style.top) elem.style.top="0px";
	var xpos=parseInt(elem.style.left);
	var ypos=parseInt(elem.style.top);
	if(xpos==final_x && ypos==final_y){
		return true;
		alert(final_x);
	}
	if(xpos<final_x){
		var dist=Math.ceil((final_x-xpos)/72);
		xpos=xpos+dist;
	}
	if(xpos>final_x){
		var dist=Math.ceil((xpos-final_x)/72);
		xpos=xpos-dist;
	}
	if(ypos<final_y){
		var dist=Math.ceil((final_y-ypos)/72);
		ypos+dist;
	}
	if(ypos>final_y){
		var dist=Math.ceil((ypos-final_y)/72);
		ypos=ypos-dist;
	}
	elem.style.left=xpos+"px";
	elem.style.top=ypos+"px";
	var repeat="moveElement('"+elementID+"',"+final_x+","+final_y+","+interval+")";
	elem.movement=setTimeout(repeat,interval);
}
function removeAllSpace(str){//删除所有空格
	var localString = '';
	for(var index = 0; index<str.length; index++){
		if(str.charCodeAt(index)!= 32){
			localString += str.charAt(index);
		}
	}
	return localString;
}

function replaceSpace(str){//利用正则表达式删除空格
	return str.replace(/\s/g,'');
}

String.prototype.Trim = function()
{
	return this.replace(/(^\s*)|(\s*$)/g, "");
}
String.prototype.LTrim = function()
{
	return this.replace(/(^\s*)/g, "");
}
String.prototype.RTrim = function()
{
	return this.replace(/(\s*$)/g, "");
}

function addClass(element,value){
	if(!element.className){
		element.className=value;
	}else{
		newClassName=element.className;
		newClassName+=" ";
		newClassName+=value;
		element.className=newClassName;
	}
}

/*
function UrlEncode(str)   
{   
    return transform(str);   
}   
  
function transform(s)   
{   
    var hex=''  
    var i,j,t   
  
    j=0   
    for (i=0; i<s.length; i++)   
    {   
        t = hexfromdec( s.charCodeAt(i) );   
        if (t=='25')   
        {   
            t='';   
        }   
        hex += '%' + t;   
    }   
    return hex;   
}   
  
function hexfromdec(num) {   
        if (num > 65535) { return ("err!") }   
        first = Math.round(num/4096 - .5);   
        temp1 = num - first * 4096;   
        second = Math.round(temp1/256 -.5);   
        temp2 = temp1 - second * 256;   
        third = Math.round(temp2/16 - .5);   
        fourth = temp2 - third * 16;   
        return (""+getletter(third)+getletter(fourth));   
}   
  
function getletter(num) {   
        if (num < 10) {   
                return num;   
        }   
        else {   
            if (num == 10) { return "A" }   
            if (num == 11) { return "B" }   
            if (num == 12) { return "C" }   
            if (num == 13) { return "D" }   
            if (num == 14) { return "E" }   
            if (num == 15) { return "F" }   
        }   
}
*/

function SetCookie(name,value,expires,path,domain,secure)
{
	var expDays = expires*24*60*60*1000;
	var expDate = new Date();
	expDate.setTime(expDate.getTime()+expDays);
	var expString = ((expires==null) ? "": (";expires="+expDate.toGMTString()));
	var pathString = ((path==null) ? "": (";path="+path));
	var domainString = ((domain==null) ? "": (";domain="+domain));
	var secureString = ((secure==true) ? ";secure": "");
	document.cookie = name + "="+ escape(value) + expString + pathString + domainString + secureString;
}

function GetCookie(name)
{
	var result = null;
	var myCookie = document.cookie + ";";
	var searchName = name + "=";
	var startOfCookie = myCookie.indexOf(searchName);
	var endOfCookie;
	if (startOfCookie != -1)
	{
		startOfCookie += searchName.length;
		endOfCookie = myCookie.indexOf(";",startOfCookie);
		result = unescape(myCookie.substring(startOfCookie,endOfCookie));
	}
	return result;
}

function ClearCookie(name)
{
	var ThreeDays=3*24*60*60*1000;
	var expDate = new Date();
	expDate.setTime(expDate.getTime()-ThreeDays);
	document.cookie=name+"=;expires="+expDate.toGMTString();
} 

function ShowContent(iframid,obj){
   var theframe=document.getElementById(iframid);
   var href=obj.getAttribute("href");
   theframe.src=href;
   return false;
}

String.prototype.ReplaceAll = stringReplaceAll;

function  stringReplaceAll(AFindText,ARepText){
  raRegExp = new RegExp(AFindText,"g");
  return this.replace(raRegExp,ARepText);
}

function MyEscape(str)   
{   
    if (str == null || str == "")   
    {   
        return str;   
    }   
    var value = escape(str);   
    //替换+号为%2B   
    value = value.replace(/\+/g, "%2B");   
    //替换%号为%25如"%u2a2a dfd %u1f1f";替换后为%25u2a2a dfd %25u1f1f   
    value = value.replace(/%u([0-9A-F]{4})/ig,function(word){return escape(word);});        
    return value;   
} 

//function getElementsByClass(parentNode){
	//var par=parentNode.getElement
//}

function getXmlDoc(retXml) { 
       if( retXml == "false" ){
        //can not get the customer info
			alert(retXml);
			return false;
       }else{
       
			//get the xml data
			var xmlDoc;
			if (window.ActiveXObject)
			{
				   xmlDoc=new ActiveXObject("Microsoft.XMLDOM");
				   xmlDoc.async=true;
				   xmlDoc.loadXML(retXml);
			}
			// code for Mozilla, Firefox, Opera, etc.
			else if (document.implementation && document.implementation.createDocument)
			{
			  var oParser=new DOMParser(); 
			  xmlDoc=oParser.parseFromString(retXml,"text/xml");

			}
			else
			{
				   alert('你的浏览器不支持这个脚本！');
				   return false;
			}
	   }
	   return xmlDoc;
}

function setStyle(cssObj,href) {
	returnNode(cssObj).setAttribute("href",href);
}

function getStyle(cssObj,href) {
	var style=returnNode(cssObj).getAttribute("href");
	return style;
}