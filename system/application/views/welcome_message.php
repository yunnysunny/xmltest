<html>
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<title>Search in form XML</title>

<style type="text/css">

body {
 background-color: #fff;
 margin: 40px;
 font-family: Lucida Grande, Verdana, Sans-serif;
 font-size: 14px;
 color: #4F5155;
}

a {
 color: #003399;
 background-color: transparent;
 font-weight: normal;
}

h1 {
 color: #444;
 background-color: transparent;
 border-bottom: 1px solid #D0D0D0;
 font-size: 16px;
 font-weight: bold;
 margin: 24px 0 2px 0;
 padding: 5px 0 6px 0;
}

code {
 font-family: Monaco, Verdana, Sans-serif;
 font-size: 12px;
 background-color: #f9f9f9;
 border: 1px solid #D0D0D0;
 color: #002166;
 display: block;
 margin: 14px 0 14px 0;
 padding: 12px 10px 12px 10px;
}

td.data {
	 font-family: Monaco, Verdana, Sans-serif;
	 font-size: 12px;
	 background-color: #f9f9f9;
	 border: 1px solid #D0D0D0;
	 color: #002166;
	 display: block;
	 margin: 14px 0 14px 0;
	 padding: 12px 10px 12px 10px;
}

</style>
<script language="javascript" src="<? echo base_url();?>js/dom.js"></script>
<script language="javascript" src="<? echo base_url();?>js/ajax.js"></script>
<script language="javascript" >
	var base_url="<? echo base_url();?>";
</script>
<script language="javascript" src="<? echo base_url();?>js/xml.js"></script>
</head>
<body>

<h1>Search in form XML</h1>
<?php
if(is_array($types)) {
?>

<p>
<select id="type" onchange="showData();">
	<option value="-1" selected>请选择查询类型</option>
	<?php
	foreach($types as $type) {
		echo "<option value='".$type['tid']."'>".$type['value']."</option>";
	}
	?>
</select>
</p>
<p>
	<table id="msg">
	</table>
</p>
<code>控制器地址：system/application/controllers/welcome.php</code>
<?
} else {
?>
<p>抱歉，获取信息失败</p>
<?
}
?>
<p><br />页面执行时间：Page rendered in {elapsed_time} seconds</p>

</body>
</html>