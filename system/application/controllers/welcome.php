<?php

class Welcome extends Controller {

	function Welcome()
	{
		parent::Controller();
		$this->load->model('xmlopr');
	}
	
	function index()
	{
		$types=$this->xmlopr->getAllType();
		$data=array('types'=>$types);
		$this->load->view('welcome_message',$data);
	}

	function fetchData() {
		$type=$this->input->post('type',true);
		$doc = new DOMDocument("1.0","utf-8");
        $doc->formatOutput = true;
		$root = $doc->createElement("root");//根节点
		$this->db->where('f_tid',$type);
		$result=$this->db->get('xml_data');
		if($result->num_rows()>0) {
			$rows=$result->result_array();
			foreach($rows as $row) {
				$data=$doc->createElement("data");
				
				foreach($row as $key => $value) {
					$child=$doc->createElement($key);
					$child->appendChild($doc->createTextNode($value));
					$data->appendChild($child);
				}

				$root->appendChild($data);
			}
		} 
		$doc->appendChild( $root );
		echo $doc->saveXML();	

	}
}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */